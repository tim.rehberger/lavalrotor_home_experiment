"""Functions for processing measurement data"""

import numpy as np
from numpy.fft import fft
from typing import Tuple


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    abs_val = np.zeros(len(x))
    
    for i, value in enumerate(x):
        abs_val[i] = (value**2 + y[i]**2 + z[i]**2)**0.5
        
    return abs_val

def interpolation(time: np.ndarray, data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Linearly interpolates values in data.

    Uses linear Newtonian interpolation. The interpolation points are distributed linearly over the
    entire time (min(time) to max(time)).

    Args:
        time (ndarray): Timestamp of the values in data.
        data (ndarray): Values to interpolate.

    Returns:
        (ndarray): Interpolation points based on 'time'.
        (ndarray): Interpolated values based on 'data'.
    """
    
    n = len(time)
    time_start = time[0]
    time_end = time[n - 1]
    time_avg = (time_end - time_start) / n
    
    time_list = list(time)
    data_list = list(data)
    
    new_time = []
    new_values = []
    current_time = time[0]
    
    # The first value stays the same
    
    new_time.append(time[0])
    new_values.append(data[0])
    
    n -= 1
    i = 0
    
    while i < n:
        
        current_time = current_time + time_avg
        
        # find the next timestamp with index j that is bigger or equal to the current point of interpolation
        
        for j, t in enumerate(time_list):
            
            if t >= current_time:
                break
        
        # Interplation between measurement point j and the previous point j - 1
        # m is the gradient between these two measurements
        # t_diff is the time difference between the new interpolated point and the measurement point to the left of that point
        
        m = (data_list[j] - data_list[j - 1]) / (time_list[j] - time_list[j - 1])
        t_diff = current_time - time_list[j - 1]
        
        new_values.append(m * t_diff + data_list[j - 1]) 
        new_time.append(current_time)
        
        # Removing points that have already been used for interpolation
        # check if the loop has moved to the next point
        # if j is 1 we are still between the same 2 points, meaning there is a bigger gap between these measurements
        # if j is 2 we have moved one point further regarding the measurement and can safely remove the first point
        # if j is bigger than 2 we have jumped over some points, meaning there are smaller gaps in the measurement
        
        while j > 1: 
            time_list.pop(0)
            data_list.pop(0)
            j -= 1
            
        i += 1
            
    
    # The last value stays the same
    
    new_time.append(current_time + time_avg)
    new_values.append(data[n])
    
    return (np.array(new_time), np.array(new_values))
    


def my_fft(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x using the numpy function fft()

    Args:
        x (ndarray): Measurement data that is transformed into the frequency range.
        time (ndarray): Timestamp of the measurement data.

    Returns:
        (ndarray): Amplitude of the computed FFT spectrum.
        (ndarray): Frequency of the computed FFT spectrum.
    """
    
    
    X = fft(x)
    N = len(X)
    n = np.arange(N)
    sr = 1 / time[1] - time[0]
    T = N/sr
    freq = n/T
    
    return [X, freq]